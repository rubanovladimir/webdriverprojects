/*Данный автотест открывает сайт дуолинго в полноэкранном режиме в google chrome*/

package org.itstep.qa;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeOpen {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("https://ru.duolingo.com/");
        driver.manage().window().maximize();

    }
}
