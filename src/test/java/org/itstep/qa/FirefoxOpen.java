/*Данный автотест открывает сайт stackoverflow в полноэкранном режиме в firefox*/

package org.itstep.qa;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxOpen {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver","src/main/resources/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://stackoverflow.com/");
        driver.manage().window().maximize();

    }
}
